package com.messenger.android.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.messenger.android.messages.LatestMessagesActivity
import com.messenger.android.MyLibrary
import com.messenger.android.R

class LoginFragment : Fragment() {

    companion object {
        const val TAG = "LoginFragment"
    }

    private lateinit var regLink: AppCompatTextView
    private lateinit var textMain: AppCompatTextView

    private lateinit var emailInput: TextInputLayout
    private lateinit var passwordInput: TextInputLayout

    private lateinit var loginBtn: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view  = inflater.inflate(R.layout.fragment_login, container, false)

        regLink = view.findViewById(R.id.regLink)
        regLink.text = MyLibrary.setTextHTML(
            resources.getString(R.string.signup_link)
        )

        textMain = view.findViewById(R.id.login_text_main)
        textMain.text = MyLibrary.setTextHTML(
            resources.getString(R.string.login_text_main)
        )

        emailInput = view.findViewById(R.id.emailInput)
        passwordInput = view.findViewById(R.id.passwordInput)

        loginBtn = view.findViewById(R.id.login_mainButton)
        loginBtn.setOnClickListener {
            performLogin()
        }

        return view
    }

    private fun performLogin() {
        val email = emailInput.editText?.text.toString().trim()
        val password = passwordInput.editText?.text.toString().trim()

        if (!MyLibrary.validateEmail(email, emailInput, view!!.context) or
                !MyLibrary.validatePassword(password, passwordInput, view!!.context))
            return

        loginBtn.text = resources.getText(R.string.login_text_button_main_load)
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) {
                    return@addOnCompleteListener
                }
                Log.d(TAG, "Successfully logged in: ${it.result?.user?.uid}")

                val intent = Intent(context, LatestMessagesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener {
                Log.d(TAG, "Failed to log in: ${it.message}")
                loginBtn.text = resources.getText(R.string.login_text_button_main)

                showSnack(view, resources.getString(R.string.fb_unsuccessful_log))
            }

    }

    private fun showSnack(view: View?, message: CharSequence) {
        val snack = Snackbar.make(view!!, message, Snackbar.LENGTH_LONG)
        snack.show()
    }


}
