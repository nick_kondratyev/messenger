package com.messenger.android.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.messenger.android.R
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        val authAdapter = AuthAdapter(supportFragmentManager)

        pager_auth.adapter = authAdapter
        pager_auth.currentItem = 0


        //changing status bar
//        if (android.os.Build.VERSION.SDK_INT >= 21) {
//            val window = this.window
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.statusBarColor = resources.getColor(R.color.textColor_white)
//        }
    }

//    //Switch fragments
//    fun switchFragmet(view: View) {
//        when(view.id) {
//
//        }
//    }
}
