package com.messenger.android.auth

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.messenger.android.messages.LatestMessagesActivity
import com.messenger.android.MyLibrary
import com.messenger.android.R
import com.messenger.android.models.User
import de.hdodenhof.circleimageview.CircleImageView
import java.lang.Exception
import java.util.*

class SignupFragment : Fragment() {

    companion object {
        const val TAG = "SignupFragment"
    }

    private lateinit var logLink: AppCompatTextView
    private lateinit var textMain: AppCompatTextView

    private lateinit var userInput: TextInputLayout
    private lateinit var emailinput: TextInputLayout
    private lateinit var passwordinput: TextInputLayout

    private lateinit var signupBtn: Button

    private var selectPhotoUri: Uri? = null
    private lateinit var selectPhoto: ImageButton
    private lateinit var selectPhotoCircle: CircleImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_signup, container, false)

        userInput = view.findViewById(R.id.userInput)
        emailinput = view.findViewById(R.id.emailInput)
        passwordinput = view.findViewById(R.id.passwordInput)

        logLink = view.findViewById(R.id.logLink)
        logLink.text = MyLibrary.setTextHTML(
            resources.getString(R.string.login_link)
        )

        textMain = view.findViewById(R.id.signup_text_main)
        textMain.text = MyLibrary.setTextHTML(
            resources.getString(R.string.signup_text_main)
        )

        selectPhoto = view.findViewById(R.id.selectPhoto)

        //Press on SelectPhoto Button
        selectPhoto.setOnClickListener {
            Log.d(TAG, "Try to show photo selector")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        signupBtn = view.findViewById(R.id.signup_mainButton)

        //Press on SignUp Button
        signupBtn.setOnClickListener {
            performSignup()
        }

        selectPhotoCircle = view.findViewById(R.id.selectPhotoCircleSignup)



        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
                Log.d(TAG, "Photo was selected")

                selectPhotoUri = data.data
                val bitmap =
                    MediaStore.Images.Media.getBitmap(context?.contentResolver, selectPhotoUri)
                selectPhotoCircle.setImageBitmap(bitmap)

                selectPhoto.alpha = 0f
            }
        } catch (e: Exception) {
            Log.d(TAG, "Exception: $e")
            showSnack(view, resources.getString(R.string.error_007))
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun performSignup() {
        val user = userInput.editText?.text.toString().trim()
        val email = emailinput.editText?.text.toString().trim()
        val password = passwordinput.editText?.text.toString().trim()

        Log.d(TAG, "onPressed SignupBtn")
        Log.d(TAG, "username is : $user")
        Log.d(TAG, "email is : $email")
        Log.d(TAG, "password is : $password")

        if (!MyLibrary.validateUsername(user, userInput, view!!.context) or
            !MyLibrary.validateEmail(email, emailinput, view!!.context) or
            !MyLibrary.validatePassword(password, passwordinput, view!!.context)
        ) {
            return
        } else {

            if (selectPhotoUri == null) {
                showSnack(view, resources.getString(R.string.fb_add_photo_reg))
                return
            }

            signupBtn.text = resources.getText(R.string.signup_text_button_main_load)

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (!it.isSuccessful) {
                        return@addOnCompleteListener
                    }

                    Log.d(TAG, "Successfully created user with uid: ${it.result?.user?.uid}")

                    uploadImageToFireBaseStorage()
                }
                .addOnFailureListener {
                    Log.d(TAG, "Failed to created user: ${it.message}")
                    signupBtn.text = resources.getText(R.string.signup_text_button_main)
                    showSnack(view, resources.getString(R.string.fb_unsuccessful_reg))
                }
        }
    }

    private fun uploadImageToFireBaseStorage() {
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")


        ref.putFile(selectPhotoUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Successfully uploaded images: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d(TAG, "File location: $it")

                    saveUserToFireBaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                // do some logging here
            }

    }

    private fun saveUserToFireBaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val userText = userInput.editText?.text.toString().trim()
        val user = User(uid, userText, profileImageUrl)

        ref.setValue(user)
            .addOnSuccessListener {
                Log.d(TAG, "!!-->>>Finally we saved the user to FireBase Database!<<<--!!")
                signupBtn.text = resources.getText(R.string.signup_text_button_main)

                val intent = Intent(context, LatestMessagesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)

            }
            .addOnFailureListener {
                Log.d(TAG, "Failed to set value to DataBase: ${it.message}")
            }

    }

    private fun showSnack(view: View?, message: CharSequence) {
        val snack = Snackbar.make(view!!, message, Snackbar.LENGTH_LONG)
        snack.show()
    }

}
