package com.messenger.android.views

import com.messenger.android.R
import com.messenger.android.models.ChatMessage
import com.messenger.android.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.user_row_latest_message.view.*
import java.util.*


class LatestMessageRow(private val chatMessage: ChatMessage) : Item<ViewHolder>() {

    var chatPartnerUser: User? = null

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.latest_message.text = chatMessage.text

        viewHolder.itemView.data_last_mes.text = getDate(chatMessage.timeStamp).substring(0, 3)

        val chatPartnerId: String = if (chatMessage.fromId == FirebaseAuth.getInstance().uid) {
            chatMessage.toId
        } else {
            chatMessage.fromId
        }

        val ref = FirebaseDatabase.getInstance().getReference("/users/$chatPartnerId")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                chatPartnerUser = p0.getValue(User::class.java)
                viewHolder.itemView.username_latest_message.text = chatPartnerUser?.username

                val targetImageView = viewHolder.itemView.imageview_latest_message
                Picasso.get().load(chatPartnerUser?.profileImageUrl).into(targetImageView)
            }
        })

    }

    override fun getLayout(): Int {
        return R.layout.user_row_latest_message
    }

    //format data from DB
    private fun getDate(time: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time

        return calendar.time.toString()
    }

}
