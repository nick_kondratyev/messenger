package com.messenger.android

/**
 * @author Nick Kondratyev
 */

import android.content.Context
import android.content.pm.PackageManager
import android.text.Html
import android.text.Spanned
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern


class MyLibrary {
    companion object {

        //Validation users
        private const val MIN_LENGTH_USERNAME: Int = 8
        private const val MAX_LENGTH_USERNAME: Int = 20
        private const val MIN_LENGTH_PASSWORD: Int = 6
        private const val MAX_LENGTH_PASSWORD: Int = 20

        private val usernamePattern: Pattern = Pattern.compile(
            "^(?=.{8,20}\$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])\$"
        )

        private val emailPattern: Pattern = Pattern.compile(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        )

        private val passwordPattern: Pattern = Pattern.compile(
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,20}$"
        )

        fun validateUsername(input: String, text: TextInputLayout, context: Context): Boolean {
            return when {
                input.isEmpty() -> {
                    text.error = context.resources.getText(R.string.error_001)
                    false
                }
                input.isNotEmpty() && input.length > MAX_LENGTH_USERNAME -> {
                    text.error = context.resources.getText(R.string.error_004)
                    false
                }
                input.isNotEmpty() && input.length < MIN_LENGTH_USERNAME -> {
                    text.error = context.resources.getText(R.string.error_010)
                    false
                }
                !usernamePattern.matcher(input).find() -> {
                    text.error = context.resources.getText(R.string.error_011)
                    false
                }
                else -> {
                    text.error = null
                    true
                }
            }
        }

        fun validateEmail(input: String, text: TextInputLayout, context: Context): Boolean {
            return when {
                input.isEmpty() -> {
                    text.error = context.resources.getText(R.string.error_001)
                    false
                }
                !emailPattern.matcher(input).find() -> {
                    text.error = context.resources.getText(R.string.error_006)
                    false
                }
                else -> {
                    text.error = null
                    true
                }
            }
        }

        fun validatePassword(input: String, text: TextInputLayout, context: Context): Boolean {
            return when {
                input.isEmpty() -> {
                    text.error = context.resources.getText(R.string.error_001)
                    false
                }
                input.length > MAX_LENGTH_PASSWORD -> {
                    text.error = context.resources.getText(R.string.error_002)
                    false
                }
                input.isNotEmpty() && input.length < MIN_LENGTH_PASSWORD -> {
                    text.error = context.resources.getText(R.string.error_003)
                    false
                }
                !passwordPattern.matcher(input).find() -> {
                    text.error = context.resources.getText(R.string.error_008)
                    false
                }
                else -> {
                    text.error = null
                    true
                }
            }
        }

        //Html-format
        fun setTextHTML(source: String): Spanned {
            val result: Spanned = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(source)
            }

            return result
        }

        //getVersion
        fun getVersion(context: Context): String? {
            val manager = context.packageManager
            val info = manager.getPackageInfo(context.packageName, PackageManager.GET_ACTIVITIES)
            return info.versionName
        }

    }

}