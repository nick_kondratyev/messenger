package com.messenger.android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityOptionsCompat
import com.google.firebase.auth.FirebaseAuth
import com.messenger.android.auth.AuthActivity
import com.messenger.android.messages.LatestMessagesActivity

class SplashScreen : AppCompatActivity() {

    companion object {
        const val TIME_OUT: Long = 1800
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed(Runnable {
            kotlin.run {
                verifyUserIsLoggedIn()
                finish()
            }
        }, TIME_OUT)
    }


    private fun verifyUserIsLoggedIn() {
        val bundle = ActivityOptionsCompat.makeCustomAnimation(
            this, android.R.anim.fade_in,
            android.R.anim.fade_out
        ).toBundle()


        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, AuthActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent, bundle)
        } else {
            val intent = Intent(this, LatestMessagesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent, bundle)
        }
    }
}
